import { Button, Input } from "antd";
import * as React from "react";
import { setUser } from "../redux/sotre";

interface MainMenuProps {}
const MainMenu: React.FunctionComponent<MainMenuProps> = (props) => {
  const [username, setUsername] = React.useState("");

  return (
    <div style={{ float: "left", margin: "20px" }}>
      {"Insert name"}
      <Input
        style={{ width: "300px" }}
        placeholder="Insert your name"
        value={username}
        onChange={(e) => {
          setUsername(e.target.value);
        }}
      />
      <Button
        onClick={() => {
          setUser(username);
        }}
      >
        Start
      </Button>
    </div>
  );
};

export default MainMenu;
