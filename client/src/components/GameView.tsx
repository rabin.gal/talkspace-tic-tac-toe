import * as React from "react";
import { useSelector } from "react-redux";
import { reduxSteateType } from "../redux/sotre";
import BoardRow from "./BoardRow";

interface GameViewProps {}
const GameView: React.FunctionComponent<GameViewProps> = (props) => {
  const board = useSelector((state: reduxSteateType) => state.board);
  return (
    <div>
      {board.map((row: any, i) => (
        <BoardRow key={i} row={row} rowNumber={i} />
      ))}
    </div>
  );
};

export default GameView;
