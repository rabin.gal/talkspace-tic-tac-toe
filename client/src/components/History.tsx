import { Table } from "antd";
import * as React from "react";
import { SERVER_TOP_RESULTS } from "../statics";

const columns = [
  {
    title: "User",
    dataIndex: "username",
    key: "username",
  },
  {
    title: "Points",
    dataIndex: "points",
    key: "points",
  },
];
interface HistoryProps {}
const History: React.FunctionComponent<HistoryProps> = (props) => {
  const [data, setData] = React.useState([]);
  React.useEffect(() => {
    fetch(SERVER_TOP_RESULTS, {
      method: "get",
    }).then((res) => res.json().then((json) => setData(json)));
  }, []);
  return <Table columns={columns} dataSource={data} />;
};

export default History;
