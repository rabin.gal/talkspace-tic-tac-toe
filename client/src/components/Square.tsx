import * as React from "react";
import { MouseEventHandler } from "react";

interface SquareProps {
  onClick: MouseEventHandler<HTMLButtonElement>;
  value: string;
}
const Square: React.FunctionComponent<SquareProps> = (props) => {
  return (
    <button
      className="square"
      onClick={props.onClick}
      style={{
        width: "50px",
        height: "50px",
        padding: "0px",
        textAlign: "center",
        float: "left",
      }}
    >
      {props.value}
    </button>
  );
};

export default Square;
