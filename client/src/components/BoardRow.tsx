import * as React from "react";
import { useSelector } from "react-redux";
import {
  changeTurn,
  reduxSteateType,
  setBoard,
  setGameResult,
} from "../redux/sotre";
import { ePlayerSign, eStatus, SERVER_NEXT_MOVE_EP } from "../statics";
import Square from "./Square";
interface BoardRowProps {
  row: string[];
  rowNumber: number;
}
const BoardRow: React.FunctionComponent<BoardRowProps> = (props) => {
  const board = useSelector((state: reduxSteateType) => state.board);
  const playerTurn = useSelector((state: reduxSteateType) => state.playerTurn);
  const gameResult = useSelector((state: reduxSteateType) => state.result);
  const username = useSelector((state: reduxSteateType) => state.username);
  return (
    <div
      style={{
        display: "table",
      }}
    >
      {props.row.map((v, i) => {
        return (
          <Square
            key={i}
            value={board[props.rowNumber][i]}
            onClick={async () => {
              if (!board[props.rowNumber][i] && !gameResult) {
                //User turn
                console.log(`User click on cell: ${props.rowNumber},${i}`);
                const copyBoard = JSON.parse(JSON.stringify(board));
                copyBoard[props.rowNumber][i] = playerTurn;
                setBoard(copyBoard);
                changeTurn();
                console.log(`waiting for server`);
                //Server turn
                const result = await fetch(SERVER_NEXT_MOVE_EP, {
                  method: "put",
                  body: JSON.stringify({
                    serverSign: ePlayerSign.player2,
                    userSign: ePlayerSign.player1,
                    mat: copyBoard,
                    username,
                  }),
                  headers: { "Content-Type": "application/json" },
                });
                const json = await result.json();
                const gameStatus: eStatus = json.status;
                if (gameStatus === eStatus.serverWin) setGameResult("Losser!");
                else if (gameStatus === eStatus.userWin) setGameResult("Win!!");
                else if (gameStatus === eStatus.tie) setGameResult("Draw");
                setBoard(json.board);
                changeTurn();
              }
            }}
          />
        );
      })}
    </div>
  );
};

export default BoardRow;
