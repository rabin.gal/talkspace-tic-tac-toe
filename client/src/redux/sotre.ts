import { createStore } from "redux";
import { BOARD_SIZE, ePlayerSign } from "../statics";

export const CHANGE_BOARD_CELL = "changebordcell";
export const WHO_TURN = "whoturn";
export const GAME_RESULT = "gameresult";
export const USER_NAME = "username";

export interface reduxSteateType {
  board: string[][];
  playerTurn: ePlayerSign;
  result: string;
  username: string;
}
const initialState = {
  board: Array(BOARD_SIZE).fill(Array(BOARD_SIZE).fill("")),
  playerTurn: ePlayerSign.player1,
  result: "",
  username: "",
};

function changeReducer(
  state: reduxSteateType = initialState,
  action: { type: string; value: any }
) {
  switch (action.type) {
    case USER_NAME:
      return { ...state, username: action.value };
    case GAME_RESULT:
      return { ...state, result: action.value };
    case CHANGE_BOARD_CELL:
      return { ...state, board: action.value };
    case WHO_TURN:
      return {
        ...state,
        playerTurn:
          state.playerTurn === ePlayerSign.player1
            ? ePlayerSign.player2
            : ePlayerSign.player1,
      };
    default:
      return state;
  }
}
export const store = createStore(changeReducer);
function render() {
  const state = store.getState();
}
store.subscribe(render);

export const setBoard = (board: string[][]) => {
  store.dispatch({
    type: CHANGE_BOARD_CELL,
    value: board,
  });
};
export const changeTurn = () => {
  store.dispatch({
    type: WHO_TURN,
    value: undefined,
  });
};
export const setGameResult = (msg: string) => {
  store.dispatch({
    type: GAME_RESULT,
    value: msg,
  });
};
export const setUser = (username: string) => {
  store.dispatch({
    type: USER_NAME,
    value: username,
  });
};
