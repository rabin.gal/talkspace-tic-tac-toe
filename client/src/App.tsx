import React from "react";
import { useSelector } from "react-redux";
import "./App.css";
import GameView from "./components/GameView";
import History from "./components/History";
import MainMenu from "./components/MainMenu";
import { reduxSteateType } from "./redux/sotre";

function App() {
  const gameResult = useSelector((state: reduxSteateType) => state.result);
  const username = useSelector((state: reduxSteateType) => state.username);
  return (
    <div>
      {username ? <GameView /> : <MainMenu />}
      <History />
      <span style={{ color: "red" }}>{gameResult}</span>
    </div>
  );
}

export default App;
