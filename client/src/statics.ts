const SERVER_PATH = "http://localhost:3101";
export const SERVER_NEXT_MOVE_EP = SERVER_PATH + "/next";
export const SERVER_TOP_RESULTS = SERVER_PATH + "/top";

export const BOARD_SIZE = 3;
export enum ePlayerSign {
  player1 = "X",
  player2 = "O",
}

export enum eStatus {
  inProcess = 1,
  serverWin,
  userWin,
  tie,
}
