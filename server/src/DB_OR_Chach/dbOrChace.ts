//I dont have time to implement it
// in real word we can save the result in some database or save it in server chach
const dbMock: { username: string; points: number }[] = [];

export const addUserPoint = (username: string, point: number) => {
  const index = dbMock.findIndex((x) => x.username === username);
  if (index > -1) {
    dbMock[index].points += point;
  } else dbMock.push({ username, points: point });
};

export const getTop = (top: number = 10) => {
  const sorted = dbMock.sort((a, b) => b.points - a.points);
  if (sorted.length > top) return sorted.slice(0, top - 1);

  return sorted;
};
