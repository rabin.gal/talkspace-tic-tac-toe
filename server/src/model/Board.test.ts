import { ePlayerSign } from "../../statics";
import Board from "./Board";
const matrixSize = 3;

const createNewBoard = () => {
  return new Array(matrixSize)
    .fill(undefined)
    .map(() => new Array(matrixSize).fill(undefined));
};
describe("Check winner", () => {
  test("Empty Board - no winner", async () => {
    const matt = createNewBoard();
    const board = new Board(matt);
    expect(board.checkWinner(ePlayerSign.player1)).toBe(false);
    expect(board.checkWinner(ePlayerSign.player2)).toBe(false);
    expect(board.isBoardFull()).toBe(false);
  });
  test("No winner", async () => {
    const matt = createNewBoard();
    const board = new Board(matt);
    matt[0][0] = ePlayerSign.player1;
    matt[0][1] = ePlayerSign.player2;
    matt[0][2] = ePlayerSign.player1;
    matt[1][0] = ePlayerSign.player2;
    matt[1][1] = ePlayerSign.player1;
    matt[1][2] = ePlayerSign.player2;
    expect(board.checkWinner(ePlayerSign.player1)).toBe(false);
    expect(board.checkWinner(ePlayerSign.player2)).toBe(false);
    expect(board.isBoardFull()).toBe(false);
  });
  test("First Row Win", async () => {
    const matt = createNewBoard();
    matt[0][0] = ePlayerSign.player1;
    matt[0][1] = ePlayerSign.player1;
    matt[0][2] = ePlayerSign.player1;
    matt[1][2] = ePlayerSign.player2;
    matt[2][2] = ePlayerSign.player2;
    const board = new Board(matt);
    expect(board.checkWinner(ePlayerSign.player1)).toBe(true);
    expect(board.checkWinner(ePlayerSign.player2)).toBe(false);
    expect(board.isBoardFull()).toBe(false);
  });
  test("Sec Row Win", async () => {
    const matt = createNewBoard();
    matt[1][0] = ePlayerSign.player2;
    matt[1][1] = ePlayerSign.player2;
    matt[1][2] = ePlayerSign.player2;
    matt[0][2] = ePlayerSign.player1;
    matt[2][2] = ePlayerSign.player1;
    const board = new Board(matt);
    expect(board.checkWinner(ePlayerSign.player2)).toBe(true);
    expect(board.checkWinner(ePlayerSign.player1)).toBe(false);
    expect(board.isBoardFull()).toBe(false);
  });
  test("Third Row Win", async () => {
    const matt = createNewBoard();
    matt[2][0] = ePlayerSign.player2;
    matt[2][1] = ePlayerSign.player2;
    matt[2][2] = ePlayerSign.player2;
    matt[0][1] = ePlayerSign.player1;
    matt[1][1] = ePlayerSign.player1;
    matt[1][0] = ePlayerSign.player1;
    const board = new Board(matt);
    expect(board.checkWinner(ePlayerSign.player2)).toBe(true);
    expect(board.checkWinner(ePlayerSign.player1)).toBe(false);
    expect(board.isBoardFull()).toBe(false);
  });
  test("Firs Col Win", async () => {
    const matt = createNewBoard();
    matt[0][2] = ePlayerSign.player2;
    matt[1][2] = ePlayerSign.player2;
    matt[2][2] = ePlayerSign.player2;
    matt[2][1] = ePlayerSign.player1;
    matt[2][0] = ePlayerSign.player1;
    matt[1][1] = ePlayerSign.player1;
    const board = new Board(matt);
    expect(board.checkWinner(ePlayerSign.player2)).toBe(true);
    expect(board.checkWinner(ePlayerSign.player1)).toBe(false);
    expect(board.isBoardFull()).toBe(false);
  });
  test("Sec Col Win", async () => {
    const matt = createNewBoard();
    matt[0][1] = ePlayerSign.player2;
    matt[2][1] = ePlayerSign.player2;
    matt[1][1] = ePlayerSign.player2;
    matt[2][0] = ePlayerSign.player1;
    matt[2][2] = ePlayerSign.player1;
    matt[1][0] = ePlayerSign.player1;
    const board = new Board(matt);
    expect(board.checkWinner(ePlayerSign.player2)).toBe(true);
    expect(board.checkWinner(ePlayerSign.player1)).toBe(false);
    expect(board.isBoardFull()).toBe(false);
  });
  test("Third Col Win", async () => {
    const matt = createNewBoard();
    matt[0][0] = ePlayerSign.player2;
    matt[1][0] = ePlayerSign.player2;
    matt[2][0] = ePlayerSign.player2;
    matt[2][1] = ePlayerSign.player1;
    matt[2][2] = ePlayerSign.player1;
    matt[1][1] = ePlayerSign.player1;
    const board = new Board(matt);
    expect(board.checkWinner(ePlayerSign.player2)).toBe(true);
    expect(board.checkWinner(ePlayerSign.player1)).toBe(false);
    expect(board.isBoardFull()).toBe(false);
  });
  test("Main Slant Win", async () => {
    const matt = createNewBoard();
    matt[0][0] = ePlayerSign.player2;
    matt[1][1] = ePlayerSign.player2;
    matt[2][2] = ePlayerSign.player2;
    matt[2][1] = ePlayerSign.player1;
    matt[2][0] = ePlayerSign.player1;
    matt[0][1] = ePlayerSign.player1;
    const board = new Board(matt);
    expect(board.checkWinner(ePlayerSign.player2)).toBe(true);
    expect(board.checkWinner(ePlayerSign.player1)).toBe(false);
    expect(board.isBoardFull()).toBe(false);
  });
  test("Sec Slant Win", async () => {
    const matt = createNewBoard();
    matt[0][2] = ePlayerSign.player2;
    matt[1][1] = ePlayerSign.player2;
    matt[2][0] = ePlayerSign.player2;
    matt[2][1] = ePlayerSign.player1;
    matt[2][2] = ePlayerSign.player1;
    matt[0][1] = ePlayerSign.player1;
    const board = new Board(matt);
    expect(board.checkWinner(ePlayerSign.player2)).toBe(true);
    expect(board.checkWinner(ePlayerSign.player1)).toBe(false);
    expect(board.isBoardFull()).toBe(false);
  });
});

describe("Check is board full", () => {
  it("Should be full - test 1", async () => {
    const matt = createNewBoard();
    matt[0][0] = ePlayerSign.player2;
    matt[0][1] = ePlayerSign.player1;
    matt[0][2] = ePlayerSign.player2;
    matt[1][0] = ePlayerSign.player2;
    matt[1][1] = ePlayerSign.player1;
    matt[1][2] = ePlayerSign.player1;
    matt[2][0] = ePlayerSign.player1;
    matt[2][1] = ePlayerSign.player2;
    matt[2][2] = ePlayerSign.player2;
    const board = new Board(matt);
    expect(board.checkWinner(ePlayerSign.player1)).toBe(false);
    expect(board.checkWinner(ePlayerSign.player2)).toBe(false);
    expect(board.isBoardFull()).toBe(true);
  });
  it("Should be full - test 2", async () => {
    const matt = createNewBoard();
    matt[0][0] = ePlayerSign.player2;
    matt[0][1] = ePlayerSign.player2;
    matt[0][2] = ePlayerSign.player1;
    matt[1][0] = ePlayerSign.player1;
    matt[1][1] = ePlayerSign.player2;
    matt[1][2] = ePlayerSign.player2;
    matt[2][0] = ePlayerSign.player2;
    matt[2][1] = ePlayerSign.player1;
    matt[2][2] = ePlayerSign.player1;
    const board = new Board(matt);
    expect(board.checkWinner(ePlayerSign.player1)).toBe(false);
    expect(board.checkWinner(ePlayerSign.player2)).toBe(false);
    expect(board.isBoardFull()).toBe(true);
  });
});
