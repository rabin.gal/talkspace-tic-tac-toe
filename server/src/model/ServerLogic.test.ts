import { ePlayerSign } from "../../statics";
import Board from "./Board";
import ServerLogic from "./ServerLogic";
const matrixSize = 3;

const createNewBoard = () => {
  return new Array(matrixSize)
    .fill(undefined)
    .map(() => new Array(matrixSize).fill(undefined));
};
describe("Block Competitor", () => {
  test("When board is empty", async () => {
    const matt = createNewBoard();
    const board = new Board(matt);
    expect(new ServerLogic(ePlayerSign.player1, board).blockCompetitor()).toBe(
      undefined
    );
    expect(new ServerLogic(ePlayerSign.player2, board).blockCompetitor()).toBe(
      undefined
    );
  });

  it("When board is full - test 1", async () => {
    const matt = createNewBoard();
    matt[0][0] = ePlayerSign.player2;
    matt[0][1] = ePlayerSign.player1;
    matt[0][2] = ePlayerSign.player2;
    matt[1][0] = ePlayerSign.player2;
    matt[1][1] = ePlayerSign.player1;
    matt[1][2] = ePlayerSign.player1;
    matt[2][0] = ePlayerSign.player1;
    matt[2][1] = ePlayerSign.player2;
    matt[2][2] = ePlayerSign.player2;
    const board = new Board(matt);
    expect(board.checkWinner(ePlayerSign.player1)).toBe(false);
    expect(board.checkWinner(ePlayerSign.player2)).toBe(false);
    expect(board.isBoardFull()).toBe(true);

    expect(new ServerLogic(ePlayerSign.player1, board).blockCompetitor()).toBe(
      undefined
    );
    expect(new ServerLogic(ePlayerSign.player2, board).blockCompetitor()).toBe(
      undefined
    );
  });
  it("When board is full - test 2", async () => {
    const matt = createNewBoard();
    matt[0][0] = ePlayerSign.player2;
    matt[0][1] = ePlayerSign.player2;
    matt[0][2] = ePlayerSign.player1;
    matt[1][0] = ePlayerSign.player1;
    matt[1][1] = ePlayerSign.player2;
    matt[1][2] = ePlayerSign.player2;
    matt[2][0] = ePlayerSign.player2;
    matt[2][1] = ePlayerSign.player1;
    matt[2][2] = ePlayerSign.player1;
    const board = new Board(matt);
    expect(board.checkWinner(ePlayerSign.player1)).toBe(false);
    expect(board.checkWinner(ePlayerSign.player2)).toBe(false);
    expect(board.isBoardFull()).toBe(true);
    expect(new ServerLogic(ePlayerSign.player1, board).blockCompetitor()).toBe(
      undefined
    );
    expect(new ServerLogic(ePlayerSign.player2, board).blockCompetitor()).toBe(
      undefined
    );
  });
});

describe("Make not a loss", () => {
  it("Should be block in first row", async () => {
    const matt = createNewBoard();
    matt[0][0] = ePlayerSign.player2;
    matt[0][1] = ePlayerSign.player2;
    const board = new Board(matt);
    expect(board.checkWinner(ePlayerSign.player1)).toBe(false);
    expect(board.checkWinner(ePlayerSign.player2)).toBe(false);
    expect(board.isBoardFull()).toBe(false);
    const point = new ServerLogic(ePlayerSign.player1, board).blockCompetitor();
    expect(point).not.toBe(undefined);
    expect(point.row).toBe(0);
    expect(point.col).toBe(2);
  });
  it("Should be block in second row", async () => {
    const matt = createNewBoard();
    matt[1][0] = ePlayerSign.player2;
    matt[1][2] = ePlayerSign.player2;
    const board = new Board(matt);
    expect(board.checkWinner(ePlayerSign.player1)).toBe(false);
    expect(board.checkWinner(ePlayerSign.player2)).toBe(false);
    expect(board.isBoardFull()).toBe(false);
    const point = new ServerLogic(ePlayerSign.player1, board).blockCompetitor();
    expect(point).not.toBe(undefined);
    expect(point.row).toBe(1);
    expect(point.col).toBe(1);
  });
  it("Should be block in third row", async () => {
    const matt = createNewBoard();
    matt[2][1] = ePlayerSign.player2;
    matt[2][2] = ePlayerSign.player2;
    const board = new Board(matt);
    expect(board.checkWinner(ePlayerSign.player1)).toBe(false);
    expect(board.checkWinner(ePlayerSign.player2)).toBe(false);
    expect(board.isBoardFull()).toBe(false);
    const point = new ServerLogic(ePlayerSign.player1, board).blockCompetitor();
    expect(point).not.toBe(undefined);
    expect(point.row).toBe(2);
    expect(point.col).toBe(0);
  });
  it("Should be block in first col", async () => {
    const matt = createNewBoard();
    matt[0][0] = ePlayerSign.player2;
    matt[1][0] = ePlayerSign.player2;
    const board = new Board(matt);
    expect(board.checkWinner(ePlayerSign.player1)).toBe(false);
    expect(board.checkWinner(ePlayerSign.player2)).toBe(false);
    expect(board.isBoardFull()).toBe(false);
    const point = new ServerLogic(ePlayerSign.player1, board).blockCompetitor();
    expect(point).not.toBe(undefined);
    expect(point.row).toBe(2);
    expect(point.col).toBe(0);
  });
  it("Should be block in second col", async () => {
    const matt = createNewBoard();
    matt[1][1] = ePlayerSign.player2;
    matt[2][1] = ePlayerSign.player2;
    const board = new Board(matt);
    expect(board.checkWinner(ePlayerSign.player1)).toBe(false);
    expect(board.checkWinner(ePlayerSign.player2)).toBe(false);
    expect(board.isBoardFull()).toBe(false);
    const point = new ServerLogic(ePlayerSign.player1, board).blockCompetitor();
    expect(point).not.toBe(undefined);
    expect(point.row).toBe(0);
    expect(point.col).toBe(1);
  });
  it("Should be block in third col", async () => {
    const matt = createNewBoard();
    matt[0][2] = ePlayerSign.player2;
    matt[2][2] = ePlayerSign.player2;
    const board = new Board(matt);
    expect(board.checkWinner(ePlayerSign.player1)).toBe(false);
    expect(board.checkWinner(ePlayerSign.player2)).toBe(false);
    expect(board.isBoardFull()).toBe(false);
    const point = new ServerLogic(ePlayerSign.player1, board).blockCompetitor();
    expect(point).not.toBe(undefined);
    expect(point.row).toBe(1);
    expect(point.col).toBe(2);
  });
  it("Should be block in main slant - test 1", async () => {
    const matt = createNewBoard();
    matt[0][0] = ePlayerSign.player2;
    matt[1][1] = ePlayerSign.player2;
    const board = new Board(matt);
    expect(board.checkWinner(ePlayerSign.player1)).toBe(false);
    expect(board.checkWinner(ePlayerSign.player2)).toBe(false);
    expect(board.isBoardFull()).toBe(false);
    const point = new ServerLogic(ePlayerSign.player1, board).blockCompetitor();
    expect(point).not.toBe(undefined);
    expect(point.row).toBe(2);
    expect(point.col).toBe(2);
  });
  it("Should be block in main slant - test 2", async () => {
    const matt = createNewBoard();
    matt[2][2] = ePlayerSign.player2;
    matt[1][1] = ePlayerSign.player2;
    const board = new Board(matt);
    expect(board.checkWinner(ePlayerSign.player1)).toBe(false);
    expect(board.checkWinner(ePlayerSign.player2)).toBe(false);
    expect(board.isBoardFull()).toBe(false);
    const point = new ServerLogic(ePlayerSign.player1, board).blockCompetitor();
    expect(point).not.toBe(undefined);
    expect(point.row).toBe(0);
    expect(point.col).toBe(0);
  });
  it("Should be block in main slant - test 3", async () => {
    const matt = createNewBoard();
    matt[0][0] = ePlayerSign.player2;
    matt[2][2] = ePlayerSign.player2;
    const board = new Board(matt);
    expect(board.checkWinner(ePlayerSign.player1)).toBe(false);
    expect(board.checkWinner(ePlayerSign.player2)).toBe(false);
    expect(board.isBoardFull()).toBe(false);
    const point = new ServerLogic(ePlayerSign.player1, board).blockCompetitor();
    expect(point).not.toBe(undefined);
    expect(point.row).toBe(1);
    expect(point.col).toBe(1);
  });
  it("Should be block in second slant - test 1", async () => {
    const matt = createNewBoard();
    matt[0][2] = ePlayerSign.player2;
    matt[1][1] = ePlayerSign.player2;
    const board = new Board(matt);
    expect(board.checkWinner(ePlayerSign.player1)).toBe(false);
    expect(board.checkWinner(ePlayerSign.player2)).toBe(false);
    expect(board.isBoardFull()).toBe(false);
    const point = new ServerLogic(ePlayerSign.player1, board).blockCompetitor();
    expect(point).not.toBe(undefined);
    expect(point.row).toBe(2);
    expect(point.col).toBe(0);
  });
  it("Should be block in second slant - test 2", async () => {
    const matt = createNewBoard();
    matt[0][2] = ePlayerSign.player2;
    matt[2][0] = ePlayerSign.player2;
    const board = new Board(matt);
    expect(board.checkWinner(ePlayerSign.player1)).toBe(false);
    expect(board.checkWinner(ePlayerSign.player2)).toBe(false);
    expect(board.isBoardFull()).toBe(false);
    const point = new ServerLogic(ePlayerSign.player1, board).blockCompetitor();
    expect(point).not.toBe(undefined);
    expect(point.row).toBe(1);
    expect(point.col).toBe(1);
  });
  it("Should be block in second slant - test 3", async () => {
    const matt = createNewBoard();
    matt[1][1] = ePlayerSign.player2;
    matt[2][0] = ePlayerSign.player2;
    const board = new Board(matt);
    expect(board.checkWinner(ePlayerSign.player1)).toBe(false);
    expect(board.checkWinner(ePlayerSign.player2)).toBe(false);
    expect(board.isBoardFull()).toBe(false);
    const point = new ServerLogic(ePlayerSign.player1, board).blockCompetitor();
    expect(point).not.toBe(undefined);
    expect(point.row).toBe(0);
    expect(point.col).toBe(2);
  });
});

describe("Last one and i win", () => {
  it("Should win in first row", async () => {
    const matt = createNewBoard();
    const serverSign = ePlayerSign.player1;
    const userSign = ePlayerSign.player2;
    matt[0][0] = serverSign;
    matt[0][1] = serverSign;
    const board = new Board(matt);
    expect(board.checkWinner(ePlayerSign.player1)).toBe(false);
    expect(board.checkWinner(ePlayerSign.player2)).toBe(false);
    expect(board.isBoardFull()).toBe(false);
    const point = new ServerLogic(serverSign, board).canIWin(userSign);
    expect(point).not.toBe(undefined);
    expect(point.row).toBe(0);
    expect(point.col).toBe(2);
  });
  it("Should win in second row", async () => {
    const matt = createNewBoard();
    const serverSign = ePlayerSign.player1;
    const userSign = ePlayerSign.player2;
    matt[1][0] = serverSign;
    matt[1][2] = serverSign;
    const board = new Board(matt);
    expect(board.checkWinner(ePlayerSign.player1)).toBe(false);
    expect(board.checkWinner(ePlayerSign.player2)).toBe(false);
    expect(board.isBoardFull()).toBe(false);
    const point = new ServerLogic(serverSign, board).canIWin(userSign);
    expect(point).not.toBe(undefined);
    expect(point.row).toBe(1);
    expect(point.col).toBe(1);
  });
  it("Should win in third row", async () => {
    const matt = createNewBoard();
    const serverSign = ePlayerSign.player1;
    const userSign = ePlayerSign.player2;
    matt[2][1] = serverSign;
    matt[2][2] = serverSign;
    const board = new Board(matt);
    expect(board.checkWinner(ePlayerSign.player1)).toBe(false);
    expect(board.checkWinner(ePlayerSign.player2)).toBe(false);
    expect(board.isBoardFull()).toBe(false);
    const point = new ServerLogic(serverSign, board).canIWin(userSign);
    expect(point).not.toBe(undefined);
    expect(point.row).toBe(2);
    expect(point.col).toBe(0);
  });
  it("Should win in first col", async () => {
    const matt = createNewBoard();
    const serverSign = ePlayerSign.player1;
    const userSign = ePlayerSign.player2;
    matt[0][0] = serverSign;
    matt[1][0] = serverSign;
    const board = new Board(matt);
    expect(board.checkWinner(ePlayerSign.player1)).toBe(false);
    expect(board.checkWinner(ePlayerSign.player2)).toBe(false);
    expect(board.isBoardFull()).toBe(false);
    const point = new ServerLogic(serverSign, board).canIWin(userSign);
    expect(point).not.toBe(undefined);
    expect(point.row).toBe(2);
    expect(point.col).toBe(0);
  });
  it("Should win in second col", async () => {
    const matt = createNewBoard();
    const serverSign = ePlayerSign.player1;
    const userSign = ePlayerSign.player2;
    matt[1][1] = serverSign;
    matt[2][1] = serverSign;
    const board = new Board(matt);
    expect(board.checkWinner(ePlayerSign.player1)).toBe(false);
    expect(board.checkWinner(ePlayerSign.player2)).toBe(false);
    expect(board.isBoardFull()).toBe(false);
    const point = new ServerLogic(serverSign, board).canIWin(userSign);
    expect(point).not.toBe(undefined);
    expect(point.row).toBe(0);
    expect(point.col).toBe(1);
  });
  it("Should win in third col", async () => {
    const matt = createNewBoard();
    const serverSign = ePlayerSign.player1;
    const userSign = ePlayerSign.player2;
    matt[0][2] = serverSign;
    matt[2][2] = serverSign;
    const board = new Board(matt);
    expect(board.checkWinner(ePlayerSign.player1)).toBe(false);
    expect(board.checkWinner(ePlayerSign.player2)).toBe(false);
    expect(board.isBoardFull()).toBe(false);
    const point = new ServerLogic(serverSign, board).canIWin(userSign);
    expect(point).not.toBe(undefined);
    expect(point.row).toBe(1);
    expect(point.col).toBe(2);
  });
  it("Should win in main slant - test 1", async () => {
    const matt = createNewBoard();
    const serverSign = ePlayerSign.player1;
    const userSign = ePlayerSign.player2;
    matt[0][0] = serverSign;
    matt[1][1] = serverSign;
    const board = new Board(matt);
    expect(board.checkWinner(ePlayerSign.player1)).toBe(false);
    expect(board.checkWinner(ePlayerSign.player2)).toBe(false);
    expect(board.isBoardFull()).toBe(false);
    const point = new ServerLogic(serverSign, board).canIWin(userSign);
    expect(point).not.toBe(undefined);
    expect(point.row).toBe(2);
    expect(point.col).toBe(2);
  });
  it("Should win in main slant - test 2", async () => {
    const matt = createNewBoard();
    const serverSign = ePlayerSign.player1;
    const userSign = ePlayerSign.player2;
    matt[2][2] = serverSign;
    matt[1][1] = serverSign;
    const board = new Board(matt);
    expect(board.checkWinner(ePlayerSign.player1)).toBe(false);
    expect(board.checkWinner(ePlayerSign.player2)).toBe(false);
    expect(board.isBoardFull()).toBe(false);
    const point = new ServerLogic(serverSign, board).canIWin(userSign);
    expect(point).not.toBe(undefined);
    expect(point.row).toBe(0);
    expect(point.col).toBe(0);
  });
  it("Should win in main slant - test 3", async () => {
    const matt = createNewBoard();
    const serverSign = ePlayerSign.player1;
    const userSign = ePlayerSign.player2;
    matt[0][0] = serverSign;
    matt[2][2] = serverSign;
    const board = new Board(matt);
    expect(board.checkWinner(ePlayerSign.player1)).toBe(false);
    expect(board.checkWinner(ePlayerSign.player2)).toBe(false);
    expect(board.isBoardFull()).toBe(false);
    const point = new ServerLogic(serverSign, board).canIWin(userSign);
    expect(point).not.toBe(undefined);
    expect(point.row).toBe(1);
    expect(point.col).toBe(1);
  });
  it("Should win in second slant - test 1", async () => {
    const matt = createNewBoard();
    const serverSign = ePlayerSign.player1;
    const userSign = ePlayerSign.player2;
    matt[0][2] = serverSign;
    matt[1][1] = serverSign;
    const board = new Board(matt);
    expect(board.checkWinner(ePlayerSign.player1)).toBe(false);
    expect(board.checkWinner(ePlayerSign.player2)).toBe(false);
    expect(board.isBoardFull()).toBe(false);
    const point = new ServerLogic(serverSign, board).canIWin(userSign);
    expect(point).not.toBe(undefined);
    expect(point.row).toBe(2);
    expect(point.col).toBe(0);
  });
  it("Should win in second slant - test 2", async () => {
    const matt = createNewBoard();
    const serverSign = ePlayerSign.player1;
    const userSign = ePlayerSign.player2;
    matt[0][2] = serverSign;
    matt[2][0] = serverSign;
    const board = new Board(matt);
    expect(board.checkWinner(ePlayerSign.player1)).toBe(false);
    expect(board.checkWinner(ePlayerSign.player2)).toBe(false);
    expect(board.isBoardFull()).toBe(false);
    const point = new ServerLogic(serverSign, board).canIWin(userSign);
    expect(point).not.toBe(undefined);
    expect(point.row).toBe(1);
    expect(point.col).toBe(1);
  });
  it("Should win in second slant - test 3", async () => {
    const matt = createNewBoard();
    const serverSign = ePlayerSign.player1;
    const userSign = ePlayerSign.player2;
    matt[1][1] = serverSign;
    matt[2][0] = serverSign;
    const board = new Board(matt);
    expect(board.checkWinner(ePlayerSign.player1)).toBe(false);
    expect(board.checkWinner(ePlayerSign.player2)).toBe(false);
    expect(board.isBoardFull()).toBe(false);
    const point = new ServerLogic(serverSign, board).canIWin(userSign);
    expect(point).not.toBe(undefined);
    expect(point.row).toBe(0);
    expect(point.col).toBe(2);
  });
});
