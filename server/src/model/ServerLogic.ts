import { ePlayerSign } from "../../statics";
import Board from "./Board";
import { Point } from "./interfaces/point";

export default class ServerLogic {
  serverSign: ePlayerSign;
  bord: Board;
  constructor(serverSign: ePlayerSign, bord: Board) {
    this.serverSign = serverSign;
    this.bord = bord;
  }

  private oneStepFromWining = (checkSign: ePlayerSign) => {
    const checkRowOrCol = (
      index: number,
      isRow: boolean
    ): undefined | { row: number; col: number } => {
      let blockPoint = undefined;
      for (let i = 0; i < this.bord.size; i++) {
        const cell = isRow
          ? this.bord.boardMattrix[i][index]
          : this.bord.boardMattrix[index][i];
        if (cell === checkSign) return undefined;
        if (!cell) {
          if (blockPoint) {
            return undefined; // this is the second empty cell - no action is needed
          } else {
            blockPoint = { row: isRow ? i : index, col: isRow ? index : i };
          }
        }
      }
      return blockPoint;
    };

    let mainSlant = undefined;
    let secSlant = undefined;
    for (let i = 0; i < this.bord.size; i++) {
      let point = checkRowOrCol(i, true);
      if (point) return point;
      point = checkRowOrCol(i, false);
      if (point) return point;

      //check slant (0,0) (1,1) (2,2) OR (0,2) (1,1) (2,0)
      if (mainSlant !== null) {
        if (this.bord.boardMattrix[i][i] === checkSign.valueOf())
          mainSlant = null;
        if (!this.bord.boardMattrix[i][i]) {
          if (!mainSlant) {
            mainSlant = { row: i, col: i }; //first empty cell
          } else mainSlant = null; // this is the seond empty
        }
      }

      if (secSlant !== null) {
        if (
          this.bord.boardMattrix[i][this.bord.size - 1 - i] ===
          checkSign.valueOf()
        )
          secSlant = null;
        if (!this.bord.boardMattrix[i][this.bord.size - 1 - i]) {
          if (!secSlant) {
            secSlant = { row: i, col: this.bord.size - 1 - i }; //first empty cell
          } else secSlant = null; // this is the seond empty
        }
      }
    }

    return mainSlant ? mainSlant : secSlant ? secSlant : undefined; //make sure return undefined
  };

  blockCompetitor(): undefined | { row: number; col: number } {
    return this.oneStepFromWining(this.serverSign);
  }

  canIWin(userSign: ePlayerSign): undefined | { row: number; col: number } {
    return this.oneStepFromWining(userSign);
  }

  nextMove() {
    const center = parseInt((this.bord.size / 2) as any);
    if (!this.bord.boardMattrix[center][center]) {
      //center is better
      this.bord.boardMattrix[center][center] = this.serverSign;
    } else {
      const point = this.randomPoint();
      this.bord.boardMattrix[point.row][point.col] = this.serverSign;
    }
  }

  randomPoint() {
    const freePoint = new Map<number, Point>();
    let count = 0;
    //get all free cells
    for (let row = 0; row < this.bord.size; row++) {
      for (let col = 0; col < this.bord.size; col++) {
        if (!this.bord.boardMattrix[row][col])
          freePoint.set(++count, { row, col });
      }
    }
    const rand = Math.floor(Math.random() * count + 1);
    return freePoint.get(rand);
  }
}
