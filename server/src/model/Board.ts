import { ePlayerSign, eStatus } from "../../statics";

export default class Board {
  gameStatus: eStatus;
  size: number;
  boardMattrix: string[][];
  constructor(mat: string[][]) {
    this.size = mat.length;
    this.gameStatus = eStatus.inProcess;
    if (this.size !== mat[mat.length - 1].length) {
      throw new Error("matrix size is forbidden");
    }
    this.boardMattrix = mat;
  }

  checkWinner(checkPlayer: ePlayerSign): boolean {
    const checkRowAndCol = (pointToCheck: number): boolean => {
      let rowCount = 0;
      let colCount = 0;
      for (let i = 0; i < this.size; i++) {
        if (this.boardMattrix[pointToCheck][i] === checkPlayer.valueOf())
          colCount++;

        if (this.boardMattrix[i][pointToCheck] === checkPlayer.valueOf())
          rowCount++;
      }
      return rowCount === this.size || colCount === this.size;
    };
    let isMainSlant = true;
    let isSecSlant = true;
    for (let index = 0; index < this.size; index++) {
      if (checkRowAndCol(index)) return true;
      //check slant (0,0) (1,1) (2,2) OR (0,2) (1,1) (2,0)
      if (
        isMainSlant &&
        this.boardMattrix[index][index] !== checkPlayer.valueOf()
      )
        isMainSlant = false;

      if (
        isSecSlant &&
        this.boardMattrix[index][this.size - 1 - index] !==
          checkPlayer.valueOf()
      )
        isSecSlant = false;
    }

    return isSecSlant || isMainSlant;
  }

  isBoardFull(): boolean {
    for (let row = 0; row < this.size; row++)
      for (let col = 0; col < this.size; col++)
        if (!this.boardMattrix[row][col]) {
          return false;
        }

    return true;
  }
}
