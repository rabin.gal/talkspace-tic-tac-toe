import { DRAW_POINTS, ePlayerSign, eStatus, WIN_POINTS } from "../../statics";
import { addUserPoint } from "../DB_OR_Chach/dbOrChace";
import Board from "../model/Board";
import ServerLogic from "../model/ServerLogic";

const basicSteps = (board: Board, userSign: ePlayerSign) => {
  let isUserWin = board.checkWinner(userSign);
  if (isUserWin) {
    board.gameStatus = eStatus.userWin;
  } else {
    //User did not win.
    if (board.isBoardFull()) {
      board.gameStatus = eStatus.tie; //if user did not win. this is tie (because server did not make any move)
    }
  }
};
export const nextMove = (
  mat: string[][],
  userSign: ePlayerSign,
  serverSign: ePlayerSign,
  username: string
): { status: eStatus; board: string[][] } => {
  const board = new Board(mat);
  const serverLogic = new ServerLogic(serverSign, board);
  basicSteps(board, userSign);
  if (board.gameStatus === eStatus.inProcess) {
    // user did not win and this is server turn:
    let point = serverLogic.blockCompetitor();
    if (point) {
      board.boardMattrix[point.row][point.col] = serverSign;
    } else {
      //server does not have to block.
      //check if Server can win in the next move
      point = serverLogic.canIWin(userSign);
      if (point) board.boardMattrix[point.row][point.col] = serverSign;
      else {
        //Server cannot win next move.
        serverLogic.nextMove();
      }
    }
    if (board.checkWinner(serverSign)) board.gameStatus = eStatus.serverWin;
    else if (board.isBoardFull()) board.gameStatus = eStatus.tie;
  }
  if (board.gameStatus === eStatus.userWin) addUserPoint(username, WIN_POINTS);
  else if (board.gameStatus === eStatus.tie)
    addUserPoint(username, DRAW_POINTS);

  return { status: board.gameStatus, board: board.boardMattrix };
};
