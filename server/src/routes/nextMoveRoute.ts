import { app } from "../..";
import { ePlayerSign } from "../../statics";
import { nextMove } from "../controller/cntBoard";

app.put("/next", (req, res) => {
  try {
    const body = req.body;
    const serverSign: ePlayerSign = body.serverSign;
    const userSign: ePlayerSign = body.userSign;
    const username: string = body.username;
    let matt = body.mat;
    const result = nextMove(matt, userSign, serverSign, username);
    res.status(200).send(result);
  } catch (error) {
    console.error(error);
  }
});
