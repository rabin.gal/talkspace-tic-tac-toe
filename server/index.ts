import bodyParser from "body-parser";
import cors from "cors";
import { config as loadConfigFiles } from "dotenv";
import express from "express";

loadConfigFiles();

export const port = process.env.port ? process.env.port : 3101;
export const app = express();

app.use(cors());

app.use(bodyParser.json());
app.use(express.urlencoded({ limit: "1mb", extended: true }));
app.use(express.json({ limit: "1mb" }));

app.listen(port, () =>
  console.log("Start-Express", `Listening(local) on: ${port}`)
);

require("./src/routes/nextMoveRoute");
require("./src/routes/history");
