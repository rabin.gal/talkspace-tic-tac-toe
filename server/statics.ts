export const WIN_POINTS = process.env.WIN_POINTS
  ? parseInt(process.env.WIN_POINTS)
  : 100;
export const DRAW_POINTS = process.env.DRAW_POINTS
  ? parseInt(process.env.DRAW_POINTS)
  : 10;

export enum ePlayerSign {
  player1 = "X",
  player2 = "O",
}

export enum eStatus {
  inProcess = 1,
  serverWin,
  userWin,
  tie,
}
